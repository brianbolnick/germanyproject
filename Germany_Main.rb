require 'pg'
require './Database'
require './TSP'
require './Car'

#-------------------------------------------------------------------
#-------------------------LOCAL VARS--------------------------------
$total_cost = 0
$total_transportation_cost = 0
$end_tour = ''
$best_time = 0
#-------------------------------------------------------------------
#-------------------------METHODS-----------------------------------
def calc_flights
  puts '------------------------FLIGHTS--------------------------------------'
  puts "Starting point: Nashville, TN\nFirst Destination: Chicago, IL"
  puts '3 plane tickets (self, mom, dad) @ 89 euros/each via United Airlines'
  first_flight = 3 * 89

  puts "Second flight: Chicago, IL to Frankfurt, Germany\n5 plane tickets (self, mom, dad, oma, opa) @ 657 euros/each"
  second_flight = 5 * 657

  $total_cost = first_flight + second_flight
  puts "Total cost for flights: #{$total_cost} euros"
  puts '----------------------------------------------------------------------'
end

def add_cities
  db = PostgresConnect.new
  db.connect
  begin
    #gather x and y (longitude and latitude) for each city
    db.query('city') {|row|
      name = row['city']
      x_coor = row['x_coor'].to_f
      y_coor = row['y_coor'].to_f

      city = City.new(name, x_coor, y_coor)
      Manage_Tour.add_city(city)
    }

  rescue Exception => e
    puts e.message
    puts e.backtrace.inspect
  ensure
    db.disconnect
  end
end

#-------------------------------------------------------------------
def initialize_pop
  $population = Population.new(100, true)
  evolve_pop
end

#-------------------------------------------------------------------
def evolve_pop
  $population = Genetics.evolve_population( $population )
  $population = Genetics.evolve_population( $population )
  50.times do #generate 50 random tours to select from
    $population = Genetics.evolve_population( $population )
  end

  solve_tour
end

#-------------------------------------------------------------------
def solve_tour
  solution = $population.fittest.to_s
  $end_tour = convert_cities(solution)
  get_total_dist($end_tour)
end

#-------------------------------------------------------------------
#Convert final tour to array to calculate distance in km
def convert_cities(solution)
  @all_cities = []
  @current_city = ''
  counter = 0
  solution.each_char { |x|
    if counter == 0
      counter += 1
      next
    elsif x != '|'
      @current_city << x
    else
      @all_cities << @current_city
      @current_city = ''
    end
  }
  @all_cities
end

#-------------------------------------------------------------------
def get_total_dist(array)
  db = PostgresConnect.new
  db.connect
  begin

  from_city = 0
  to_city = 1
  distance = 0
  total_distance = 0
  travel_cost = 0

  puts '----------------------------ITINERARY---------------------------------'

  array.each { |city|
    puts "************************************************\nDeparting City: #{city}\n************************************************" if city != array[array.size-1]
    start = city
    finish = array[array.index(city) + 1]

    from_city += 1
    to_city += 1
    break if finish.nil?
      column_names = db.get_columns

      column_names.each { |column|
        if column != 'Cities'
          next
        else
          db.query('distance') { |row|
            row.each { |key, value|
              if (key == start) && (row['Cities'] == finish.to_s)
                distance = value
                puts 'Hotel cost: 80 euros' #average cost of 80 euros per night
                travel_cost += calc_costs(city) + 80 if city != nil
              else
                next
              end
            }
          }
        end
      }

    puts "Distance from #{start} to #{finish}: #{distance} km"
    total_distance += distance.to_i
  }
  rescue Exception => ex
    puts ex.message
    puts ex.backtrace.inspect
  ensure
    db.disconnect
  end
  puts '----------------------------------------------------------------------'
  puts '-----------------------TRANSPORTATION INFO----------------------------'
  puts "Total Travel Cost: #{travel_cost} euros"
  $total_cost += travel_cost

  method = calc_travel_method(total_distance)
  puts '######################################################################'
  puts "Best method of travel: #{method} at #{$total_transportation_cost} euros."
  puts '######################################################################'
  finalize_tour(total_distance,$total_cost)
end

def calc_travel_method(distance=0)
  puts "********************************************\nTotal Train information\n********************************************"

    train = 349 * 5 #rails pass for 5 people for 1 month (4 weeks)
    taxi = 1.2 * 10 * 22 #assuming average of 10 miles per trip (22 trips)
    train += taxi
    train_speed = 280
    puts "Base cost: #{train} euros"
    time_travelled_train = distance / train_speed
    puts "Average total time travelled on train: #{time_travelled_train} hours"

  puts "********************************************\nTotal Petrol information\n********************************************"
    petrol = Car.new((350 * 4) + 350 + 350, 22, 1.54, 130, 'Petrol')
    puts "Base cost: #{petrol.get_base_cost} euros"
    puts "Gas cost with mileage: #{petrol.get_gas_cost(distance)} euros"
    petrol_total = petrol.get_total_cost
    puts "Total petrol rent-a-car cost: #{petrol_total} euros"
    time_travelled_car = distance / 130
    puts "Average total time travelled in car: #{time_travelled_car} hours\n"

  puts "********************************************\nTotal Diesel information\n********************************************"
    diesel = Car.new((300 * 4) + 300 + 300, 13, 1.47, 130, 'Diesel')
    puts "Base cost: #{diesel.get_base_cost} euros\n"
    puts "Gas cost with mileage: #{diesel.get_gas_cost(distance)} euros\n"
    diesel_total = diesel.get_total_cost
    puts "Total diesel rent-a-car cost: #{diesel_total} euros \n"
    puts "Average total time travelled in car: #{time_travelled_car} hours\n"

  puts '............................................'
  if petrol.get_total_cost < diesel.get_total_cost
    preferred_car = petrol
  else
    preferred_car = diesel
  end
  puts "Preferred car = #{preferred_car.to_s}"
  puts '............................................'

  if train < preferred_car.get_total_cost && time_travelled_train < time_travelled_car
    $total_transportation_cost = train
    $best_time = time_travelled_train
    'Train'
  else
    $total_transportation_cost = preferred_car.get_total_cost
    $best_time = time_travelled_car
    preferred_car.to_s
  end

end
#-------------------------------------------------------------------
def calc_costs(city=nil)
  case city
    when 'Lubeck'
      puts 'Marzipan: 10 euros'
      cost = 10
    when 'Hamburg'
      puts 'River Drive (in taxi): 24 euros'
      cost = 24
    when 'Hannover'
      puts 'iPads (5): 900 euros'
      cost = 900
    when 'Koln'
      puts 'Taxi to castle: 24 euros'
      cost = 24
    when 'Baden Baden'
      puts 'Spa day: 100 euros'
      cost = 80
    when 'Basel'
      puts 'Watches (2): 9600 euros'
      cost = 9600
    else
      cost = 0
  end
  cost
end

def finalize_tour(distance,cost)
  puts '----------------------------------------------------------------------'
  puts '-----------------------------SUMMARY----------------------------------'
  puts 'Tour:'

  puts '......................................................................'
  counter = 0
  $end_tour.each {|x|
    if counter % 6 == 0 && counter != 0 #formatting
      puts
    end
    print "#{x} -> "
    counter += 1
  }
  puts "\n......................................................................"

  current_ex_rate = 1.25
  km_to_mi = 0.62

  puts "\nFinal distance: #{distance} km (#{distance * km_to_mi} miles)"
  puts "Total Travel time (excluding flights): #{$best_time} hours"
  puts "Total cost: #{cost} euros ($#{cost * current_ex_rate})"

end

#-------------------------------------------------------------------
#-----------------------MAIN----------------------------------------
puts "Let's go to Germany!"

calc_flights
add_cities
initialize_pop

