require 'pg'
class PostgresConnect
# Establish connection
  def connect
    @connection = PG.connect(
        :dbname => 'Germany',
        :user => 'postgres',
        :password => 'password')
  end
#-------------------------------------------------------------------
  #query specified table
  def query(table)
    text = 'SELECT * FROM ' + table
    @connection.exec(text) do |result|
      result.each do |row|
        yield row if block_given?
      end
    end
  end
#-------------------------------------------------------------------
  #add all column headers into array for later use
  def get_columns
    columns = []
    names = @connection.query('SELECT * FROM distance LIMIT 1').first.keys
    names.each { |x| columns << x }
    columns
  end
#-------------------------------------------------------------------
# close connection
  def disconnect
    @connection.close
  end
end