class Car
  def initialize(base_cost=nil, kpg=nil, oil_price=nil,speed=nil, type=nil)
    @base_cost = base_cost
    @kpg = kpg
    @oil_price = oil_price
    @speed = speed
    @type = type
  end

  def get_base_cost
    @base_cost
  end

  def get_kpg
    @kpg
  end

  def get_oil_price
    @oil_price
  end

  def get_speed
    @speed
  end

  def get_gas_cost(distance)
    @oil_total = distance / @kpg * @oil_price
  end

  def get_total_cost
    @base_cost + @oil_total
  end

  def get_time_travelled(distance)
    distance / @speed
  end

  def to_s
    "#{@type}"
  end
end