#--------------------------------------------------------------------#
#------------------------CITY CLASS----------------------------------#
class City
  attr_accessor :x
  attr_accessor :y
  attr_accessor :name

  def initialize(name = nil,  x = nil, y = nil)
    self.x = x || rand * 200
    self.y = y || rand * 200
    self.name = name
  end

  #calculate distance to provided city
  def distance_to( city )
    x_distance = (self.x - city.x).abs
    y_distance = ( self.y - city.y).abs
    Math.sqrt( x_distance.abs2     + y_distance.abs2 )
  end

  def to_s
    "#{name}"
  end
end
#--------------------------------------------------------------------#
#------------------------POPULATION CLASS----------------------------#
class Population
  attr_accessor :size

  def initialize( population_size, should_initialize)
    initialize_population(population_size, should_initialize)
  end

  def get_tour( index )
    tours[index]
  end

  def fittest
    tours.max_by{ |t| t.fitness }
  end

  def save_tour( index, tour )
    tours[index] = tour
  end

  def each( offset )
    tours.each_with_index do |tour, index|
      next if index < offset
      yield tour
    end
  end

  private
  def initialize_population( population_size, should_initialize = false)
    @size = population_size
    reset_tours

    if should_initialize
      population_size.times do
        new_tour = Tour.new
        new_tour.generate_individual
        tours << new_tour
      end
    end
  end

  def tours
    @tours
  end

  def reset_tours
    @tours = []
  end
end

#--------------------------------------------------------------------#
#------------------------GENETICS CLASS------------------------------#
class Genetics
  def self.mutation_rate
    0.015
  end

  def self.tournament_size
    5
  end

  def self.elitism
    true
  end

  def self.evolve_population( population )
    new_population = Population.new( population.size, false )
    elitism_offset = 0
    if elitism
      new_population.save_tour(0, population.fittest)
      elitism_offset = 1
    end

    (elitism_offset...population.size).each do |i|

      parent1 = tournament_selection( population )
      parent2 = tournament_selection( population )

      child = crossover( parent1, parent2 )
      new_population.save_tour(i, child)
    end

    new_population.each(elitism_offset) do |tour|
      mutate(tour)
    end

    new_population
  end

  def self.crossover( parent1, parent2)
    child = Tour.new

    start_pos = Integer(rand * parent1.size)
    end_pos = Integer(rand * parent2.size)

    parent1_genes = []
    parent2_genes = []

    (0...child.size).each do |i|
      if start_pos < end_pos && i > start_pos && i < end_pos
        parent1_genes << i
        child.set_city( i, parent1.get_city(i) )
      elsif start_pos > end_pos
        unless i < start_pos && i > end_pos
          parent1_genes << i
          child.set_city(i, parent1.get_city(i))
        end
      end
    end

    parent2.each_with_index do |city, index|
      unless child.contains_city?(city)
        parent2_genes << child.set_at_first_available(city)
      end
    end

    child
  end

  def self.mutate( tour )
    (0...tour.size).each { |tourPos1|}
  end

  def self.tournament_selection( population )

    tournament = Population.new( tournament_size, false)

    (0...tournament_size).each { |index|
      random_index = Integer(rand * population.size)
      tournament.save_tour(index, population.get_tour(random_index))
    }

    tournament.fittest
  end
end

#--------------------------------------------------------------------#
#------------------------TOUR CLASS----------------------------------#
class Tour
  def initialize(tour = nil)
    if tour
      set_tour(tour)
    else
      reset_tour
    end
  end

  def generate_individual
    set_tour
    Manage_Tour.each_city { |city| tour << city }
    shuffle_tour!
  end

  def set_at_first_available(city)
    index = tour.index(nil)
    raise "No available spot left in tour! #{to_s}" unless index
    tour[index] = city
    index
  end

  def get_city(tour_position)
    tour[tour_position]
  end

  def set_city(tour_position, city)
    tour[tour_position] = city
    @fitness = @distance = 0
  end

  def fitness
    @fitness = 1.0/distance if @fitness == 0
    @fitness
  end

  def distance
    if @distance == 0
      tour_distance = 0
      tour.each_with_index { |city, index|
        from_city = city
        (index + 1 < size) ? destination_city = get_city(index + 1) : destination_city = get_city(0)
        tour_distance += from_city.distance_to(destination_city)
      }
      @distance = tour_distance
    end
    @distance
  end

  def contains_city?(city)
    tour.include?(city)
  end

  def each_with_index(&block)
    tour.each_with_index &block
  end

  def to_s
    gene_string = "|"
    tour.each { |city| gene_string << "#{city}|" }
    "#{gene_string} -> #{distance}"
  end

  def size
    @tour.size
  end

  private
  def tour
    @tour
  end

  def shuffle_tour!
    @tour = tour.shuffle
    (0..@tour.length-1).each { |i|
      if @tour[i].to_s ==('Frankfurt') #set starting city
        @first_index = i
        break
      end
    }
    @tour[@first_index] ,@tour[0] = @tour[0], @tour[@first_index]
  end

  def set_tour(tour = [])
    @fitness = 0
    @distance = 0
    @tour = tour
  end

  def reset_tour
    set_tour
    Manage_Tour.number_of_cities.times { |i| tour << nil }
  end
end

#--------------------------------------------------------------------#
#------------------------MANAGE_TOUR CLASS---------------------------#
class Manage_Tour
  def self.destinations
    @destination_cities ||= []
  end

  def self.add_city(city)
    destinations << city
  end

  def self.get_city(index)
    destinations[index]
  end

  def self.number_of_cities
    destinations.size
  end

  def display_cities
    puts @destination_cities
  end

  def self.each_city
    destinations.each { |city| yield city }
  end
end